# O Protocolo STUN (Simple Traversal of UDP through NATs)

O protocolo STUN  é baseado no modelo arquitetural cliente-servidor, operando normalmente em TCP e UDP e serve como uma ferramenta para outros protocolos, para trabalhar com tradução de endereços de rede (Network Address Translation - NAT). Pode ser usado por um "endpoint" para determinar o endereço IP e a porta alocada a ele por um NAT. Outra utilidade deste protocolo, é de verificar a conectividade entre dois pontos finais. 
O protocolo STUN trabalha com muitos endereços NATs existentes e não requer nenhum comportamento especial deles; não é uma solução de NAT por sí só. Este protocolo, é na verdade uma ferramenta para ser usada no contexto de uma solução NAT. Esta especificação de ferramenta, é uma evolução do que era antes ([RFC 3489](https://tools.ietf.org/html/rfc3489)), onde fora apresentada como uma solução completa. 
A imagem a seguir, nos dá uma ideia mais clara de como trabalha este protocolo:
![enter image description here](https://lh3.googleusercontent.com/I5JWB6ij1h2jF1xujJz0rL__3roaH1ygv2UpDZYffEaOPgUsydkrHH5hC5T8leaH3Xzzl-b5yK-g)

-   Passo 1: O computador A envia uma solicitação STUN através do gateway 192.168.1.1 para o servidor STUN fora da rede, ouvindo 64.25.58.65 usando a porta de origem 5060.
-   Passo 2: O gateway (192.168.1.1) encaminha a solicitação para o servidor STUN (64.25.58.65) e altera a porta 5060 para a porta 15060.
-   Passo 3: O servidor STUN (64.25.58.65) envia uma resposta de volta ao computador A através do gateway com o IP público 212.128.56.125, especificando que a solicitação foi recebida do IP 212.128.56.125 e da porta 15060

Este trabalho está baseado na documentação ([RFC 5389](https://tools.ietf.org/html/rfc5389)), e visa a demonstração do protocolo STUN, seus principais usos, suas principais características, bem como exemplos de como aplicá-lo. 

 - [Benefícios](https://slideplayer.com/slide/4171634/):
	 - Nenhuma alteração necessária no NAT
	 - Nenhuma alteração necessária no proxy
	 - Funciona através de Tandem NAT
	 - Funciona através do NAT mais residencial
	 - Fluxos de mídia de ponta a ponta
	 - Servidores STUN robustos
	 - Funciona para muitos aplicativos
		 - Jogos
		 - Compartilhamento de arquivos
	 - Modo Peer-to-Peer
	 
 - Principal uso:
	 - VoIP: Devido à forma como os protocolos usados ​​em uma chamada VoIP baseada em SIP funcionam, fazendo chamadas entre duas entidades SIP em execução nos dispositivos NAT, como um roteador ou firewall (cenários comuns de redes domésticas e comerciais), os usuários geralmente encontram vários problemas, como maneira de áudio durante uma chamada telefônica ou falhas de registro do telefone ao tentar se registrar para um [provedor de VoIP](https://www.3cx.com/partners/voip-providers/) ou um [PBX IP](https://www.3cx.com/) por não residir na mesma rede.  STUN é um protocolo que ajuda na resolução de problemas na implementação de VoIP. 
	 Como o STUN é uma ferramenta usada por outras aplicações, vou indicar algumas ferramentas que usam o STUN e trazem uma ideia ainda mais clara de como usá-lo:
	 
 - https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/
 - https://simpl.info/rtcpeerconnection/
 - http://phonerlite.de/config_en.htm
 - Traversal Using Relays around NAT (TURN): Relay Extensions to Session Traversal Utilities for NAT (STUN) - Rosenberg
 - https://www.process-one.net/en/ejabberd/#getejabberd
	 - Suporte para celular
	 - Arquivamento
	 - Clustering 
	 - PubSub
 - https://meet.jit.si/localhost (https://github.com/jitsi/jitsi-meet)
 - https://matrix.org/

Todas essas aplicações fazem uso do protocolo STUN para estabelecer comunicação, descobrindo o endereço de rede do outro ponto final.

 - Exemplos
	 - 
	 - O protocolo STUN funciona como um descobridor de rede, como indicado anteriormente, demostrarei alguns exemplos disso.
![fluxo](https://lh3.googleusercontent.com/cITQVrz7CuK4i_m_N3p4Bp5tamhTeMCmJC_NF81_t_xBnF4RRkdwQ9WHqFeEWCEIQGoF2AWmpfGJ)
	 
Podemos ver nessa imagem, de forma mais clara como é feito a comunicação através de um servidor STUN. Para conectar-se ao Peer B, o Peer A solicita ao servidor STUN o próprio endereço, para então fechar a conexão. Isso porque não há como um peer conectar-se a outro tendo endereços locais, o trabalho, portanto do STUN é prover essa "descoberta" de IP.

Podemos perceber isso mais claramente quando realizamos o teste de "ping"(ferramenta que permite a verificação de conexão. por meio do envio de pacotes). Estou com endereço local na minha máquina(192.168.0.114), e ainda assim consigo "pingar" o endereço stun.callwithus.com pertencente a um servidor STUN, em outra rede, mas com IP também local(192.95.17.62).
 
![enter image description here](https://lh3.googleusercontent.com/cvRkc-rFE_YRoRu3xIhQavBN1JsKhEcGqcPH4oDL5jfBXGBNrCIIgSH84D-ZdWdNEEwkYWIYDNEB)

Outro exemplo que trago aqui é o de realizar um teste, que se encontra em: [WebRTC samples Trickle ICE](https://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/).
Esta página testa a funcionalidade do ICE em uma implementação WebRTC. Ele cria um PeerConnection com os ICEServers especificados e, em seguida, inicia a coleta de candidatos para uma sessão com um único fluxo de áudio. À medida que os candidatos são reunidos, eles são exibidos na caixa de texto abaixo, junto com uma indicação quando a coleta de candidatos é concluída.

![interface da página](https://lh3.googleusercontent.com/UlhyEb2SIdmYYDsutL5IQekThvb0nZ2Zw81qsezrAeFwWIAyxTVcEmToA7jpIbzBz0tJVqIQRDkT)

